# About
Of course you already know about [MLP: The Movie](https://en.wikipedia.org/wiki/My_Little_Pony:_The_Movie_(2017_film)) leak, but when I found out that in addition to the video file of 3Gb size there is also an "original" 128Gb file (38Gb in the archive), I was interested in: how much the difference between them. So...

:link: **[MLP: The Movie quality comparasion website](https://midnightfun.gitlab.io/mlp-movie-quality-comparasion/)**

* Warning: large .png images (6Mb traffic)
* For better experience please use modern browser and wide monitor
* You can view or download the original screenshots by yourself. They are here, in the `img` folder or in `Repository -> Files`.

# Conclusion

It's a almost **placebo quality**. And it definitely is not worth it to download such a huge size file.

### But it can be OK if you:

* Want to trace some scenes with vector editor (tiny lines are more sharp and visible);
* Have unlimited disk space;
* Want to utilize your new powerful CPU or GPU;
* Just because, dude, it's a 128 Gb video file!